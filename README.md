## Simple and responsive contact form with JavaScript validation.
#### HoverCSS and AnimateCSS are being used for animations.

If you want the animations to work, you need to download the following css files: [hover-min.css](https://github.com/IanLunn/Hover/blob/master/css/hover-min.css) and [animate.min.css](https://github.com/daneden/animate.css/blob/master/animate.min.css)

If you do not want to include those files in your directory then you can also use the CDNs. If you want to use the CDNs, then in the `index.thml` file, replace:

```
<link rel="stylesheet" href="hover-min.css">
<link rel="stylesheet" href="animate.min.css">
```

with

```
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/hover.css/2.3.1/css/hover-min.css" integrity="sha256-c+C87jupO1otD1I5uyxV68WmSLCqtIoNlcHLXtzLCT0=" crossorigin="anonymous">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.7.2/animate.min.css" integrity="sha256-PHcOkPmOshsMBC+vtJdVr5Mwb7r0LkSVJPlPrp/IMpU=" crossorigin="anonymous">
```

If you want to remove all animations and just keep the form, all you need to do is remove those `<link>` tags (and the HoverCSS/AnimateCSS files if you had downloaded those) and also remove the `class="..."` attribute of the `<h2>`, the `<input>` fields and the `<button>`.